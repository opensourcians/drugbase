require 'test_helper'

class AccountDrugsControllerTest < ActionController::TestCase
  setup do
    @account_drug = account_drugs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:account_drugs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create account_drug" do
    assert_difference('AccountDrug.count') do
      post :create, account_drug: { arabic_name: @account_drug.arabic_name, commercial_name: @account_drug.commercial_name, company: @account_drug.company, discount: @account_drug.discount, drug_id: @account_drug.drug_id, drug_type: @account_drug.drug_type, factor_form: @account_drug.factor_form, international_barcode: @account_drug.international_barcode, max_limit: @account_drug.max_limit, min_limit: @account_drug.min_limit, no_meduim_units: @account_drug.no_meduim_units, price: @account_drug.price, scientific_name: @account_drug.scientific_name, temperature: @account_drug.temperature }
    end

    assert_redirected_to account_drug_path(assigns(:account_drug))
  end

  test "should show account_drug" do
    get :show, id: @account_drug
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @account_drug
    assert_response :success
  end

  test "should update account_drug" do
    patch :update, id: @account_drug, account_drug: { arabic_name: @account_drug.arabic_name, commercial_name: @account_drug.commercial_name, company: @account_drug.company, discount: @account_drug.discount, drug_id: @account_drug.drug_id, drug_type: @account_drug.drug_type, factor_form: @account_drug.factor_form, international_barcode: @account_drug.international_barcode, max_limit: @account_drug.max_limit, min_limit: @account_drug.min_limit, no_meduim_units: @account_drug.no_meduim_units, price: @account_drug.price, scientific_name: @account_drug.scientific_name, temperature: @account_drug.temperature }
    assert_redirected_to account_drug_path(assigns(:account_drug))
  end

  test "should destroy account_drug" do
    assert_difference('AccountDrug.count', -1) do
      delete :destroy, id: @account_drug
    end

    assert_redirected_to account_drugs_path
  end
end
