require 'test_helper'

class InitialBalancesControllerTest < ActionController::TestCase
  setup do
    @initial_balance = initial_balances(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:initial_balances)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create initial_balance" do
    assert_difference('InitialBalance.count') do
      post :create, initial_balance: { date_time: @initial_balance.date_time, drug_id: @initial_balance.drug_id, inventory_id: @initial_balance.inventory_id, location_layout: @initial_balance.location_layout, qunatity: @initial_balance.qunatity }
    end

    assert_redirected_to initial_balance_path(assigns(:initial_balance))
  end

  test "should show initial_balance" do
    get :show, id: @initial_balance
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @initial_balance
    assert_response :success
  end

  test "should update initial_balance" do
    patch :update, id: @initial_balance, initial_balance: { date_time: @initial_balance.date_time, drug_id: @initial_balance.drug_id, inventory_id: @initial_balance.inventory_id, location_layout: @initial_balance.location_layout, qunatity: @initial_balance.qunatity }
    assert_redirected_to initial_balance_path(assigns(:initial_balance))
  end

  test "should destroy initial_balance" do
    assert_difference('InitialBalance.count', -1) do
      delete :destroy, id: @initial_balance
    end

    assert_redirected_to initial_balances_path
  end
end
