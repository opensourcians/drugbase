Rails.application.routes.draw do

  resources :roles
	#Subdomain class auto loaded from lib (lib/subdomain.rb)
	constraints(Subdomain) do
		resources :pharmacies do
  		collection do
  			get 'search'
  		end
  	end
		devise_for :employees
		resources :employees
		resources :clients
		resources :suppliers
		resources :account_drugs
    resources :initial_balances
    resources :inventories
	end
	resources :drugs do
		collection do
			get 'search'
			get 'info'
      get 'alternatives'
		end
	end

	#get 'drugs/search' => 'drugs#search'
	get 'home' => 'home#index'
	devise_for :accounts, :controllers => {:registrations => "accounts"}
	# devise_scope :account do
	#   get 'accounts/logout'
	#   resources :accounts
	# end
	# The priority is based upon order of creation: first created -> highest priority.
	# See how all your routes lay out with "rake routes".

	# You can have the root of your site routed with "root"
	root 'home#index'
	# Example of regular route:
	#   get 'products/:id' => 'catalog#view'

	# Example of named route that can be invoked with purchase_url(id: product.id)
	#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

	# Example resource route (maps HTTP verbs to controller actions automatically):
	#   resources :products

	# Example resource route with options:
	#   resources :products do
	#     member do
	#       get 'short'
	#       post 'toggle'
	#     end
	#
	#     collection do
	#       get 'sold'
	#     end
	#   end

	# Example resource route with sub-resources:
	#   resources :products do
	#     resources :comments, :sales
	#     resource :seller
	#   end

	# Example resource route with more complex sub-resources:
	#   resources :products do
	#     resources :comments
	#     resources :sales do
	#       get 'recent', on: :collection
	#     end
	#   end

	# Example resource route with concerns:
	#   concern :toggleable do
	#     post 'toggle'
	#   end
	#   resources :posts, concerns: :toggleable
	#   resources :photos, concerns: :toggleable

	# Example resource route within a namespace:
	#   namespace :admin do
	#     # Directs /admin/products/* to Admin::ProductsController
	#     # (app/controllers/admin/products_controller.rb)
	#     resources :products
	#   end
end
