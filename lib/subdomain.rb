class Subdomain
  def self.matches?(request)
    request.subdomain.present? && Account.by_subdomain.key(request.subdomain).any?
  end

end
