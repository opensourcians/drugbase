class ApplicationPolicy
	attr_reader :user, :record

	def initialize(user, record)
		@user = user
		@record = record
	end

	def index?
		@user.class.to_s.eql?'Account' or @user.authorized? controller:  @record.to_s.pluralize.concat('Controller'), action: 'index'
	end

	def show?
		false
	end

	def create?
		false
	end

	def new?
		create?
	end

	def update?
		false
	end

	def edit?
		update?
	end

	def destroy?
		false
	end

	def scope
		Pundit.policy_scope!(user, record.model.model)
	end

	class Scope
		attr_reader :user, :scope

		def initialize(user, scope)
			@user = user
			@scope = scope
		end

		def resolve
			scope
		end
	end
end
