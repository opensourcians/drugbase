class EmployeePolicy < ApplicationPolicy
		def index?
				user.class.to_s.eql?'Account'
		end

		class Scope < Scope
				def resolve
						scope
				end
		end
end
