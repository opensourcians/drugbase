class Drug < CouchRest::Model::Base
  use_database 'drugs'
  #couchdb Views
  design do
    view :by_drug_id
    view :by_arabic_name
    view :by_international_barcode
    view :by_company ,:reduce => "_sum"
    view :by_factor_form
    view :by_price
    view :by_drug_type
    view :by_scientific_name
    view :by_international_barcode_and_commercial_name,
    :map => "function(doc) {
          if (doc.commercial_name) {
            emit(doc.commercial_name, 1)
          }
          if (doc.international_barcode) {
            emit(doc.international_barcode, 1)
          }
        }",
    :reduce => "_sum"

  end

  #Model Properties
  property :drug_id, Integer
  property :arabic_name, String
  property :commercial_name, String
  property :international_barcode, Integer
  property :company, String
  property :price, Float
  property :factor_form, String
  property :no_meduim_units, Float
  property :temperature, String
  property :scientific_name, String
  property :drug_type ,String
  property :max_limit, Integer
  property :min_limit, Integer
  property :layout_location, String
  property :discount, Float
end
