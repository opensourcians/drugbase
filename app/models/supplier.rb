class Supplier < CouchRest::Model::Base
  proxied_by :account
  design do
    view :by_name
    view :by_account_id
    view :by_account_id_and_id,
    :map => "function(doc) {
          if (doc.account_id) {
            emit([doc._id,doc.account_id], 1)
          }
        }",
    :reduce => "_sum"
  end


  validates :supplier_id,:name,:phone,:address, presence: true
  validates :supplier_id,:phone,:name, uniqueness: true
  validates :supplier_id, numericality: { only_integer: true }
  validates :phone,:telephone, format: { with: /\d{11}/, message: "bad phone number" }
  validates :closing_date,:date_of_payment_of_cheques, format: { with: /\d{2}\/\d{2}\/\d{4}/, message: "bad date" }

  #Model Properties
  property :supplier_id, Integer
  property :name, String
  property :phone, String
  property :address, String
  property :Indebtedness, Integer
  property :supplier_type, String
  property :telephone, String
  property :closing_date, String
  property :period_for_payment, String
  property :date_of_payment_of_cheques, String
  property :account_id, String

end
