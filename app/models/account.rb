class Account < CouchRest::Model::Base

	use_database 'accounts'
	proxy_database_method :subdomain
	proxy_for :pharmacies
	proxy_for :employees
	proxy_for :clients
	proxy_for :suppliers
	proxy_for :account_drugs
	proxy_for :account_operations
	proxy_for :inventories
	proxy_for :initial_balances

	#couchdb Views
	design do
		view :by_email
		view :by_subdomain
	end

	#Devise Configuration
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

	#Validations
	validates_presence_of :password, :on => :create
	validates :firstName, :lastName, :email, :subdomain, presence: true
	validates :subdomain, uniqueness: true
	validates :subdomain, length: { in: 3..15 }
	validates :subdomain, format: { with: /\A[a-z][a-z0-9]+\z/,
	message: "Must start with letter followed by numbers or letters" }
	RESERVED_SUBDOMAINS = %w(
      about abuse account accounts admin admins administrator
      administrators anonymous assets billing billings board calendar
      contact copyright e-mail email example feedback forum
      hostmaster image images inbox index invite jabber legal
      launchpad manage media messages mobile official payment
      picture pictures policy portal postmaster press privacy
      private profile search sitemap staff stage staging static
      stats status support teams username usernames users webmail
      webmaster login use jars main data application cgi app www api
      blog camo dashboard demo developer developers docs files git
      imap lab pages pop sites smtp ssl
    )

	validates :subdomain, exclusion: { in: RESERVED_SUBDOMAINS,
	message: "%{value} is reserved." }

	validates :avatar, allow_blank: true, format: {
		with: %r{\.gif|jpg|png}i,
		message: 'must be a url for gif, jpg, or png image.'
	}

	validates :logo, allow_blank: true, format: {
		with: %r{\.gif|jpg|png}i,
		message: 'must be a url for gif, jpg, or png image.'
	}
	#Model Properties
	## Database authenticatable
	property :firstName, String, :default => ""
	property :lastName, String, :default => ""
	property :email, String, :default => ""
	property :encrypted_password, String, :default => ""
	property :subdomain, String, :default => ""
	property :payment, String, :default => ""
	## Recoverable
	property :reset_password_token, String
	property :reset_password_sent_at, Time

	## Rememberable
	property :remember_created_at, Time

	## Trackable
	property :sign_in_count, Integer, default: 0
	property :current_sign_in_at, Time
	property :last_sign_in_at, Time
	property :current_sign_in_ip, String
	property :last_sign_in_ip, String
	property :avatar, String
	property :logo, String
end
