class Client < CouchRest::Model::Base
  proxied_by :account
  design do
    view :by_name
    view :by_account_id
    view :by_account_id_and_id,
    :map => "function(doc) {
          if (doc.account_id) {
            emit([doc._id,doc.account_id], 1)
          }
        }",
    :reduce => "_sum"
  end


  validates :client_id,:name,:phone,:address, presence: true
  validates :client_id,:phone,:name, uniqueness: true
  validates :client_id, numericality: { only_integer: true }
  validates :phone, format: { with: /\d{11}/, message: "bad phone number" }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "bad email format" }

  #Model Properties
  property :client_id, Integer
  property :name, String
  property :phone, String
  property :address, String
  property :email, String
  property :account_id, String

end
