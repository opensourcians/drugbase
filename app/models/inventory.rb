class Inventory < CouchRest::Model::Base
	proxied_by :account
	#couchdb Views
	design do
		view :by__id
		view :by_name
		view :by_id
	end

	#Validations
	validates :name, presence: true
	validates :address, presence: true

	#Model Properties
	property :id, String
	property :name, String
	property :address, String
	property :inventory_type, String
	property :pharmacy_id, String
end
