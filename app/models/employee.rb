class Employee < CouchRest::Model::Base

	belongs_to :role
	proxied_by :account
	design do
		view :by_email
		view :by_account_id
		view :by_account_id_and_id,
		:map => "function(doc) {
          if (doc.account_id) {
            emit([doc._id,doc.account_id], 1)
          }
        }",
		:reduce => "_sum"
	end

	devise :database_authenticatable, request_keys: [:subdomain]
	def self.find_for_authentication(tainted_conditions)
		account = Account.by_subdomain.key(tainted_conditions[:subdomain]).limit(1).first
		account.employees.by_email.key(tainted_conditions[:email]).limit(1).first
	end
	def self.serialize_into_session(record)
		[record.to_key, record.authenticatable_salt, record.account_id]
	end
	def self.serialize_from_session(key, salt, account)
		record = Account.get(account).employees.get(key.first)
		record if record && record.authenticatable_salt == salt
	end

	def authorized? params
		self.role.has_permission? params
	end

	property :firstName, String, :default => ""
	property :lastName, String, :default => ""
	property :email, String, :default => ""
	property :encrypted_password, String, :default => ""
	property :pharmacies, [String]
	property :role_id
	property :account_id, String

end
