class AccountDrug < CouchRest::Model::Base
  proxied_by :account
  design do
    view :by_company
    view :by_account_id
    view :by_account_id_and_id,
    :map => "function(doc) {
          if (doc.account_id) {
            emit([doc._id,doc.account_id], 1)
          }
        }",
    :reduce => "_sum"
  end


  validates :drug_id,:arabic_name,:commercial_name,:international_barcode, presence: true
  validates :drug_id,:arabic_name,:commercial_name,:international_barcode, uniqueness: true
  validates :discount,:price,:no_meduim_units,:max_limit,:min_limit, numericality: true
  validates :drug_id, numericality: { only_integer: true }

  validate :check_barcode_uniqueness
  def check_barcode_uniqueness
      drug=Drug.by_international_barcode.key(international_barcode)
      if drug.count > 0
        errors.add(:international_barcode, "barcode already exist")
      end
  end
  #Model Properties
  property :drug_id, Integer
  property :arabic_name, String
  property :commercial_name, String
  property :international_barcode, String
  property :company, String
  property :price, String
  property :factor_form, String
  property :no_meduim_units, Float
  property :temperature, String
  property :scientific_name, String
  property :drug_type ,String
  property :discount, Float
  property :max_limit, Integer
  property :min_limit, Integer
  property :account_id, String
  property :layout_location, String

end
