class InitialBalance < CouchRest::Model::Base
  proxied_by :account
  design do
    view :by_account_id_and_id,
    :map => "function(doc) {
          if (doc.account_id) {
            emit([doc._id,doc.account_id], 1)
          }
        }",
    :reduce => "_sum"
  end


  validates :drug_id,presence: true
  validates :drug_id,:qunatity,numericality: { only_integer: true }
  validate :check_inventory_exists
  validate :check_drug_exists

  def check_inventory_exists
      inventory=@account.inventories.by_name.key(inventory_name)
      if inventory.count <= 0
        errors.add(:inventory_id, "invalid inventory")
      end
  end

  def check_drug_exists
      drug=@account.account_drugs.by_drug_id.key(drug_id)
      if drug.count <= 0
        drug=Drug.by_drug_id.key("#{drug_id}")
        if drug.count <= 0
          errors.add(:drug_id, "invalid drug")
        end
      end
  end

  #Model Properties
  property :drug_id, Integer
  property :location_layout, String
  property :qunatity, Integer
  property :inventory_name, String
  property :account_id, String
  property :date_time, String

end
