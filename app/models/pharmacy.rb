class Pharmacy < CouchRest::Model::Base
  #use_database 'pharmacies'
  proxied_by :account
  #couchdb Views
  design do
    view :by_name
  end

  #Validations
  validates :name, presence: true
  validates :address, presence: true

  #Model Properties
  property :name, String
  property :address, String
  property :phone, String
end
