class Role < CouchRest::Model::Base
	use_database 'roles'
	design do
		view :by_name
	end
	property :name, String
	property :permissions

	validates :name, uniqueness: true
	validates :name, presence: true

	def has_permission?(options)
		self.permissions.has_key? options[:controller] and self.permissions[options[:controller]].include? options[:action]
	end

	def self.controllers_list(options = {})
		Rails.application.eager_load!
		result = ApplicationController.descendants.map(&:to_s)
		if options[:actions]
			controllers_actions = Hash.new
			ApplicationController.descendants.each do |c|
				controllers_actions.store c.to_s, c.action_methods.to_a if not c.to_s.include?'Devise'
			end
			result = controllers_actions
		end
		result
	end
end
