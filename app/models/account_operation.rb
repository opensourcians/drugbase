class AccountOperation < CouchRest::Model::Base
  proxied_by :account
  design do
    view :by_drug_id
    view :by_operation
    view :by_account_id
    view :by_account_id_and_id,
    :map => "function(doc) {
          if (doc.account_id) {
            emit([doc._id,doc.account_id], 1)
          }
        }",
    :reduce => "_sum"
  end



  #Model Properties
  property :drug_id, Integer
  property :operation, String
  property :account_id, String

end
