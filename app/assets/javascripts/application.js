// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
// Loads all Semantic javascripts
//= require semantic-ui
//= require react
//= require react_ujs
//= require components
//= require_tree .
//= require nouislider


$.fn.form.settings.rules.isUnique = function(value, isUnique) {
    var isUnique = false;
    var name = $(this).attr('name');
    var data = {validate:name,account:{}}
    data.account[name] =value;
    console.log("MyData",data);
    $.ajax({
        url: '/accounts.json',
        type: 'POST',
        data: data,
        async:false,
        success: function(data){
          console.log("DATA",data);
          isUnique = data.msg;
        }.bind(this),
        error: function(xhr, status, err) {
            console.error("Error");
        }.bind(this)
    });
    return isUnique;
};

$.fn.form.settings.rules.isStartedWithLetter = function(value, isStartedWithLetter) {
    return isStartedWithLetter= isNaN(Number(value[0])) ? true : false;
};

$.fn.form.settings.rules.isNotReserved = function(value, reservedWords) {
    return (reservedWords.split(',').indexOf(value) == -1) ? true : false;
};
