class ApplicationController < ActionController::Base

	#Authorization plugin
	include Pundit

	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	#protect_from_forgery with: :exception
	protect_from_forgery with: :null_session

	before_action :configure_permitted_parameters, if: :devise_controller?
	before_action :authenticate!

	#after_action :verify_authorized

	rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:firstName, :lastName, :subdomain,:payment])
	end

	def pundit_user
		current_account || current_employee
	end


	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:firstName, :lastName, :subdomain,:payment])
	end

	def set_account
		current_user = account_signed_in? ? current_account.id : current_employee.account_id
		@account = Account.find(current_user)
	end

	def formateNumber(number)
    first_part=''
    second_part=''
    if number.index('.') == nil
      first_part=number
      second_part="0"
      number=number+"."+second_part
    else
      first_part=number[0,number.index('.')]
      second_part=number[number.index('.')+1,number.length]
    end
    (5-first_part.length).times do |i|
      first_part="0"+first_part
    end
    (2-second_part.length).times do |i|
      second_part="0"+second_part
    end
    return first_part+"."+second_part
  end


	def authenticate!
		if request.subdomain.present? && !account_signed_in? && !request.url.include?('accounts/sign_in')
			authenticate_employee!
		else
			authenticate_account!
		end
	end

	def user_not_authorized
		flash[:alert] = "You are not authorized to perform this action."
		redirect_to(request.referrer || root_path)
	end

end
