class AccountDrugsController < ApplicationController
	before_action :set_account
	before_action :set_account_drug, only: [:show, :edit, :update, :destroy]
	# GET /account_drugs
	# GET /account_drugs.json
	def index
		#byebug
		@account_drugs = @account.account_drugs.all

	end

	# GET /account_drugs/1
	# GET /account_drugs/1.json
	def show
	end

	# GET /account_drugs/new
	def new
		@account_drug = @account.account_drugs.new
	end

	# GET /account_drugs/1/edit
	def edit
	end

	# POST /account_drugs
	# POST /account_drugs.json
	def create
		@account_drug = @account.account_drugs.new(account_drug_params)
		@account_drug.price=formateNumber(@account_drug.price)
		@operation=@account.account_operations.new(:operation=>"add",:drug_id=>@account_drug.drug_id);
		@operation.save
		respond_to do |format|
			if @account_drug.save
				format.html { redirect_to @account_drug, notice: 'Account drug was successfully created.' }
				format.json { render :show, status: :created, location: @account_drug }
			else
				format.html { render :new }
				format.json { render json: @account_drug.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /account_drugs/1
	# PATCH/PUT /account_drugs/1.json
	def update
		respond_to do |format|
			result=@account_drug.update_attributes(account_drug_params)
			if result
				format.html { redirect_to @account_drug, notice: 'Account drug was successfully updated.' }
				format.json { render :show, status: :ok, location: @account_drug }
			else
				format.html { render :edit }
				format.json { render json: @account_drug.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /account_drugs/1
	# DELETE /account_drugs/1.json
	def destroy
		# Task DRUGBASE-64
		#@account_drug = @account.account_drugs.find("2aa2a903c9cb17c86dab25f77cbc27c5")
		#if @account_drug == nil
			#@operation=@account.account_operations.new(:operation=>"delete",:drug_id=>Drug.find("2aa2a903c9cb17c86dab25f77cbc27c5").drug_id);
			#@operation.save
		#end
		@account.account_operations.by_drug_id.key(@account_drug.drug_id).first.destroy
		@account_drug.destroy
		respond_to do |format|
			format.html { redirect_to account_drugs_url, notice: 'Account drug was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_account_drug
		@account_drug = @account.account_drugs.find(params[:id])
		if @account_drug == nil
			#@account_drug=Drug.find(params[:id])
			@account_drug=@account.account_drugs.new(Drug.find(params[:id]))
		end
	end
	# Use callbacks to share common setup or constraints between actions.


	# Never trust parameters from the scary internet, only allow the white list through.
	def account_drug_params
		params.require(:account_drug).permit(:drug_id, :arabic_name, :commercial_name, :international_barcode, :company, :price, :factor_form, :no_meduim_units, :temperature, :scientific_name, :drug_type, :discount, :min_limit, :max_limit,:layout_location)
	end
end
