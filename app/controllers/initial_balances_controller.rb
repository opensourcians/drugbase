class InitialBalancesController < ApplicationController
  before_action :set_account
  before_action :set_initial_balance, only: [:show, :edit, :update, :destroy]

  # GET /initial_balances
  # GET /initial_balances.json
  def index
    @initial_balances = @account.initial_balances.all
  end

  # GET /initial_balances/1
  # GET /initial_balances/1.json
  def show
  end

  # GET /initial_balances/new
  def new
    @initial_balance = @account.initial_balances.new
    @initial_balance.date_time=Time.now
  end

  # GET /initial_balances/1/edit
  def edit
  end

  # POST /initial_balances
  # POST /initial_balances.json
  def create
    @initial_balance = @account.initial_balances.new(initial_balance_params)
    @initial_balance.date_time=Time.now
    respond_to do |format|
      if @initial_balance.save
        format.html { redirect_to @initial_balance, notice: 'Initial balance was successfully created.' }
        format.json { render :show, status: :created, location: @initial_balance }
      else
        format.html { render :new }
        format.json { render json: @initial_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /initial_balances/1
  # PATCH/PUT /initial_balances/1.json
  def update
    respond_to do |format|
      if @initial_balance.update(initial_balance_params)
        format.html { redirect_to @initial_balance, notice: 'Initial balance was successfully updated.' }
        format.json { render :show, status: :ok, location: @initial_balance }
      else
        format.html { render :edit }
        format.json { render json: @initial_balance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /initial_balances/1
  # DELETE /initial_balances/1.json
  def destroy
    @initial_balance.destroy
    respond_to do |format|
      format.html { redirect_to initial_balances_url, notice: 'Initial balance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_initial_balance
      @initial_balance = @account.initial_balances.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def initial_balance_params
      params.require(:initial_balance).permit(:drug_id, :location_layout, :inventory_name, :qunatity)
    end
end
