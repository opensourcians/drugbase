class DrugsController < ApplicationController

  before_action :set_drug, only: [:show, :edit, :update, :destroy]

  # GET /drugs
  # GET /drugs.json
  def index
    @drugs = Drug.by_drug_id.page(params[:pageNum]).per(10)
  end

  # GET /drugs/1
  # GET /drugs/1.json
  def show
  end

  # GET /drugs/?q=x


  def search
    @account = Account.find(current_account.id)
    @query=""
    @condation=""
    @my_drugs=@account.account_operations.by_operation.all
    @my_drugs=@my_drugs.map{|i| i="NOT (drug_id:#{i.drug_id.to_i})"}
    page=1
    per_page=10
    if params[:q].present?
      @words=params[:q].split(" ").map{|s| s+="*"}
      @words=@words.join(" +")
      @query='(arabic_name:(+'+@words+') OR international_barcode:(+'+@words+') OR commercial_name:(+'+@words+'))'
      @condation="AND"
    end
    if params['company'].present?
      @companies=params['company'].map{|s| s='"'+s+'"'}
      @query+=" #{@condation} company:(#{@companies.join(" ")})"

      @condation="AND"
    end
    if params['type'].present?
      @types=params['type'].map{|s| s='"'+s+'"'}
      @query+=" #{@condation} drug_type:(#{@types.join(" ")})"
      @condation="AND"
    end
    if params['form factor'].present?
      @factors=params['form factor'].map{|s| s='"'+s+'"'}
      @query+=" #{@condation} factor_form:(#{@factors.join(" ")})"
      @condation="AND"
    end
    if params['minPrice'].present?
      min=formateNumber(params['minPrice'])
      max=formateNumber(params['maxPrice'])
      @query+=" #{@condation} price:[#{min} TO #{max}]"
      @condation="AND"
    end
    if !@query.strip.present?
      @query="*:*"
    end
    @drugs = Drug.search("(#{@query.strip}) #{@my_drugs.join(" ")}").page(params[:pageNum]).per(10)
    #byebug
    records_number=@drugs.count
    if params[:pageNum].to_i * 10 > records_number
      page=(((params[:pageNum].to_i*10)- records_number).to_f/10).ceil
      per_page=(params[:pageNum].to_i*10)-records_number
    end
    #@account_drugs =@account.account_drugs.database.search("lucene/search",q:"type:AccountDrug #{@condation} #{ @query=='*:*' ? '' : @query}")['rows']
    #if per_page >= 10
    #@drugs=@drugs.all+(@account_drugs.map{|i| i=i['fields']})
    #@drugs=@drugs.paginate(:page => page, :per_page =>10)
    #end
    #byebug
    #records_number+= @account_drugs.count
  respond_to do |format|
  if records_number != 0
      format.json {render :json => {
           :records_number => records_number,
           :drugs => @drugs,
        }.to_json}
  else
    format.html { redirect_to drugs_url,drugs:null}
    format.json { render :json =>
      {
        :drugs => [],
        :records_number => 0
      }.to_json
    }
  end
end
  end


  # GET /drugs/new
  def new
    @drug = Drug.new
  end

  def info
    @companies_rows = Drug.by_company.reduce.group_level(1).rows
    @form_factors_rows = Drug.by_factor_form.reduce.group_level(1).rows
    @types_rows = Drug.by_drug_type.reduce.group_level(1).rows
    @prices_rows = Drug.by_price.reduce.group_level(1).rows
    @companies=Array.new
    @form_factors=Array.new
    @types=Array.new
    @prices=Array.new
    @companies_rows.each do |row|
      @companies.push(row.key)
    end
    @prices_rows.each do |row|
      @prices.push(row.key)
    end
    @form_factors_rows.each do |row|
      @form_factors.push(row.key)
    end
    @types_rows.each do |row|
      @types.push(row.key)
    end

    respond_to do |format|
    format.json {render :json => {
         :company => @companies,
         :"form factor" => @form_factors,
         :type => @types,
         :min_price => 0,
         :max_price => 550.0
      }.to_json}
      end
  end

  # GET /drugs/1/edit
  def edit
  end

  # POST /drugs
  # POST /drugs.json
  def create
    @drug = Drug.new(drug_params)

    respond_to do |format|
      if @drug.save
        format.html { redirect_to @drug, notice: 'Drug was successfully created.' }
        format.json { render :show, status: :created, location: @drug }
      else
        format.html { render :new }
        format.json { render json: @drug.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /drugs/1
  # PATCH/PUT /drugs/1.json
  def update
    respond_to do |format|
      if @drug.update(drug_params)
        format.html { redirect_to @drug, notice: 'Drug was successfully updated.' }
        format.json { render :show, status: :ok, location: @drug }
      else
        format.html { render :edit }
        format.json { render json: @drug.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /drugs/1
  # DELETE /drugs/1.json
  def destroy
    @drug.destroy
    respond_to do |format|
      format.html { redirect_to drugs_url, notice: 'Drug was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_drug
      @drug = Drug.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def drug_params
      params.require(:drug).permit(:commercial_name, :arabic_name, :company, :price)
    end

end
