class AccountsController < Devise::RegistrationsController
  before_action :set_account, only: [:show,:edit, :update]
  def create
    if params.has_key?:validate
      field_valid?(params[:validate])
    else
      super do |account|
        pharmacy = params.require(:pharmacy).permit(:name, :address, :phone)
        pharmacy[:account_id] = account.id
        account.pharmacies.new(pharmacy).save
      end
    end
  end
  def logout
  end
  def edit
    if params[:notice].present?
      @notice=params[:notice]
    end
  end

  def update
    respond_to do |format|
      @account.attributes = account_params
      passowrd=params[:account][:password]
      #if passowrd.present?
        #@account.password=passowrd
      #end
      picture=params[:account][:avatar]
      logo=params[:account][:logo]
      @account.avatar=set_image_path(picture,@account.avatar)
      @account.logo=set_image_path(logo,@account.logo)
      if @account.save
        @account.avatar=saveImage(picture,@account.avatar)
        @account.logo=saveImage(logo,@account.logo)
        format.html { redirect_to :action=>"edit", notice: 'account was successfully updated.' }
        format.json { render :edit, status: :ok, location: @account }
      else
        @account.avatar=Account.find(current_account.id).avatar
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  private
  def field_valid?(field)
    account = Account.new(params[:account])
    respond_to do |format|
      if account.invalid? && account.errors[field].any?
        format.json { render json: {msg: false}, status: 200 }
      else
        format.json { render json: {msg: true}, status: 200 }
      end
    end
  end

    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(current_account.id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:firstName, :lastName, :email,:subdomain,:avatar,:logo)
    end
    private

    def set_image_path(image,default)
      if image!= nil
        "#{Time.now.to_i}ـ"+image.original_filename
      else
        default
      end
    end
    def saveImage(image,default)
      if image!= nil
        File.open(Rails.root.join('public/accounts', 'images', default), 'wb') do |file|
          file.write(image.read)
        end
      end
      return default
    end
end
