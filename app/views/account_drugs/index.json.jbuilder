json.array!(@account_drugs) do |account_drug|
  json.extract! account_drug, :id, :drug_id, :arabic_name, :commercial_name, :international_barcode, :company, :price, :factor_form, :no_meduim_units, :temperature, :scientific_name, :drug_type, :discount, :min_limit, :max_limit
  json.url account_drug_url(account_drug, format: :json)
end
