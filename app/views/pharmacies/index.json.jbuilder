json.array!(@pharmacies) do |pharmacy|
	json.extract! pharmacy, :id, :name, :address, :phone
	json.url pharmacy_url(pharmacy, format: :json)
end
