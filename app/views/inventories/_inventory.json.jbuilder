json.extract! inventory, :id, :name, :address, :type, :created_at, :updated_at
json.url inventory_url(inventory, format: :json)