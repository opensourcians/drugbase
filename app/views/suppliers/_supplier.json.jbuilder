json.extract! supplier, :id, :supplier_id, :name, :phone, :address, :created_at, :updated_at
json.url supplier_url(supplier, format: :json)