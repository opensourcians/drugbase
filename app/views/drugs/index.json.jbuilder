json.records_number @drugs.count
json.drugs @drugs do |drug|
  json.extract! drug, :id, :commercial_name, :factor_form ,:no_meduim_units , :temperature ,:arabic_name, :company, :price, :international_barcode, :scientific_name, :drug_type, :discount
  json.url drug_url(drug, format: :json)
end
